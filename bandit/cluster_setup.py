import os
from itertools import product

filenames = [
    "pennmed",
    "walmart",
    "charity",
    "effort",
]
assignment_strategies = [
    "random", 
    "explorationMLE", 
    "explorationJS"
]
n_simulations = 500

if __name__ == "__main__":
    if not os.path.exists("output"):
        os.mkdir("output")
    line_iter = product(filenames, assignment_strategies, range(n_simulations))
    with open("variables.txt", "w") as f:
        f.writelines([f"{filename} {strategy} {sim_no}\n" for filename, strategy, sim_no in line_iter])
