import glob
import os

import pandas as pd

if __name__ == "__main__":
    dfs = []
    for path in glob.glob("results/*.csv"):
        try:
            dfs.append(pd.read_csv(path))
        except:
            print(f"Empty file {path}")
    pd.concat(dfs).to_csv("results.csv", index=False)
