from conditional_inference.bayes.empirical_bayes import JamesStein
from conditional_inference.conventional import ConventionalModel
from conditional_inference.quantile_unbiased.rqu import RQU

import os
import sys
import warnings
from functools import partial

import numpy as np
import pandas as pd
import statsmodels.api as sm

np.random.seed(123)

filenames = {
    "pennmed": "penn_medicine_reconstructed.csv",
    "walmart": "walmart_reconstructed.csv",
    "charity": "charity.csv",
    "effort": "effort.csv",
}
datapath = "../data"
# number of observations per batch
batch_size = 250
# total number of participants to run (may be less depending on rounding)
n_participants = 128000
# minimum number of participants at which to start collecting performance metrics
start_metrics = 2000

# define the batch intervals at which to collect metrics
n_batches = int(n_participants / batch_size)
start_metrics = int(start_metrics / batch_size)
collect_metrics = []
i = 0
while not collect_metrics or collect_metrics[-1] <= n_batches:
    collect_metrics.append(start_metrics * 2 ** i)
    i += 1
collect_metrics = collect_metrics[:-1]
n_batches = collect_metrics[-1]


def return_X_y(filename):
    # return one-hot encoded feature matrix X (n_obs, n_arms) and target vector y (n_obs,)
    df = pd.read_csv(os.path.join(datapath, filename))
    if "control" in df:
        df = df[df.control == 0]
    bootstrap_df = df.groupby("arm").apply(lambda df: df.sample(frac=1, replace=True))
    bootstrap_df = bootstrap_df.drop(columns="arm").reset_index()
    return pd.get_dummies(bootstrap_df.arm), bootstrap_df.y


def compute_sample_weight(X, y):
    # return "true" mean vector (n_arms,) and weight matrix W (n_obs, n_arms)
    # W[i, k] is the probability that you sample label i when assigning a participant to arm k
    result = sm.OLS(y, X).fit().get_robustcov_results()
    mean, cov = result.params, result.cov_params()
    mean_bar = mean.mean()
    tau = (
        ((mean - mean_bar) ** 2).sum()
        / (mean.shape[0] - 3)
        * np.identity(mean.shape[0])
    )
    projection = np.ones((mean.shape[0], mean.shape[0])) / mean.shape[0]
    weight_matrix = cov @ np.linalg.inv(tau) @ (
        projection - np.identity(mean.shape[0])
    ) + np.identity(mean.shape[0])
    sample_weight = weight_matrix.repeat(X.sum(axis=0), axis=1).T / X.sum(axis=0).values
    return weight_matrix @ mean, sample_weight


def assign_participants(
    assignment_weight, y, sample_weight, X_sample=None, y_sample=None, n_obs=batch_size
):
    # assignment_weight (n_arms,) is the proportion of participants assigned to each arm
    # n_obs is the total number of participants to assign
    assignment_weight = assignment_weight / assignment_weight.sum()
    K = assignment_weight.shape[0]
    n_obs_per_arm = np.floor(n_obs * assignment_weight)
    remaining_obs = np.random.choice(
        np.arange(K), size=int(n_obs - n_obs_per_arm.sum()), p=assignment_weight
    )
    n_obs_per_arm = (n_obs_per_arm + np.identity(K)[remaining_obs].sum(axis=0)).astype(
        int
    )

    y_new = []
    for k in range(K):
        y_new += y.sample(
            n=n_obs_per_arm[k], replace=True, weights=sample_weight[:, k]
        ).tolist()
    X_new = np.identity(K).repeat(n_obs_per_arm, axis=0)

    if X_sample is None:
        return X_new, np.array(y_new)
    return np.concatenate((X_sample, X_new), axis=0), np.concatenate((y_sample, y_new))


def compute_metrics(X, y):
    ols_results = sm.OLS(y, X).fit().get_robustcov_results()
    metrics = dict(
        n_obs=y.shape[0],
        recommended_arm=ols_results.params.argmax(),
        cum_value=y.sum(),
    )
    try:
        beta = .005
        alpha = (.05 - beta) / (1 - beta)
        rqu_dist = RQU.from_results(ols_results).get_distribution(beta=beta)
        metrics.update(dict(
            median=rqu_dist.median(),
            ppf025=rqu_dist.ppf(alpha / 2),
            ppf975=rqu_dist.ppf(1 - alpha / 2),
            mdb=rqu_dist.ppf(alpha),
        ))
    except:
        pass
    return metrics


def random_assignment(X, y, sample_weight):
    results = []
    assignment_weight = np.ones(X.shape[1])
    X_sample, y_sample = None, None
    total_batches = start_metrics
    prev_total_batches = 0
    i = 0
    while total_batches <= n_batches:
        i += 1
        n_obs = batch_size * (total_batches - prev_total_batches)
        prev_total_batches = total_batches
        total_batches = start_metrics * 2**i

        X_sample, y_sample = assign_participants(assignment_weight, y, sample_weight, X_sample=X_sample, y_sample=y_sample, n_obs=n_obs)
        results.append(compute_metrics(X_sample, y_sample))

    return results

def exploration_sampling(X, y, sample_weight, estimate_pr_bestarm):
    results = []
    assignment_weight = np.ones(X.shape[1])
    X_sample, y_sample = None, None
    for i in range(n_batches):
        X_sample, y_sample = assign_participants(assignment_weight, y, sample_weight, X_sample=X_sample, y_sample=y_sample)
        ols_results = sm.OLS(y_sample, X_sample).fit().get_robustcov_results()
        pr_bestarm = estimate_pr_bestarm(ols_results)
        assignment_weight = pr_bestarm * (1 - pr_bestarm)
        if i+1 in collect_metrics:
            results.append(compute_metrics(X_sample, y_sample))

    return results

def mle_bestarm(ols_results):
    try:
        mle_results = ConventionalModel.from_results(ols_results).fit()
        return mle_results.rank_matrix.values[0]
    except:
        # most likely error is that matrix isn't invertible => all arms equally likely to be best
        return np.full(ols_results.params.shape, 1 / ols_results.params.shape[0])

def js_bestarm(ols_results):
    with warnings.catch_warnings(record=True):
        warnings.simplefilter("error")
        try:
            js_results = JamesStein.from_results(ols_results).fit()
            return js_results.rank_matrix.values[0]
        except:
            # JamesStein prior is approximately 0 ==> all arms are equally likely to be best
            return np.full(ols_results.params.shape, 1 / ols_results.params.shape[0])

exploration_sampling_mle = partial(
    exploration_sampling, estimate_pr_bestarm=mle_bestarm
)
exploration_sampling_js = partial(exploration_sampling, estimate_pr_bestarm=js_bestarm)

assignment_strategies = {
    "random": random_assignment,
    "explorationMLE": exploration_sampling_mle,
    "explorationJS": exploration_sampling_js,
}

def run_simulation(filename_key, strategy_key, sim_no):
    filename = filenames[filename_key]
    strategy = assignment_strategies[strategy_key]
    sample_weight, i = None, 0
    while sample_weight is None or (sample_weight < 0).any():
        # use bootstrapping to get feature matrix and target vector
        # X is (n_obs, n_arms) one-hot encoded matrix, y is (n_obs,) target vector
        X, y = return_X_y(filename)
        # true_mean is (n_arms,) vector of "true" means for the sample
        # sample_weight is (n_obs, n_arms) where sample_weight[i, k] is the weight observation i gets when assigning a participants to arm k
        true_mean, sample_weight = compute_sample_weight(X, y)
        i += 1
        if i == 500:
            print("Failed to find suitable sample weights")
            return pd.DataFrame()

    df = pd.DataFrame(strategy(X, y, sample_weight))
    df["filename"] = filename_key
    df["strategy"] = strategy_key
    df["sim_no"] = sim_no
    df["recommended_rank"] = df.recommended_arm.apply(lambda idx: (true_mean > true_mean[idx]).sum())
    df["recommended_value"] = true_mean[df.recommended_arm]
    df["best_value"] = true_mean.max()
    return df

if __name__ == "__main__":
    filename_key, strategy_key, sim_no = sys.argv[1], sys.argv[2], int(sys.argv[3])
    np.random.seed(sim_no)
    df = run_simulation(filename_key, strategy_key, sim_no)
    if not os.path.exists("results"):
        os.mkdir("results")
    df.to_csv(f"results/results_{filename_key}_{strategy_key}_{sim_no}.csv", index=False)
