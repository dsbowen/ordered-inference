# Conditional inference

[![pipeline status](https://gitlab.com/dsbowen/ordered-inference/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/ordered-inference/-/commits/master)
[![coverage report](https://gitlab.com/dsbowen/ordered-inference/badges/master/coverage.svg)](https://gitlab.com/dsbowen/ordered-inference/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/dsbowen/ordered-inference/-/blob/master/LICENSE)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fordered-inference/HEAD?urlpath=lab)

A statistics package for comparing multiple policies or treatments. [Read the docs](https://dsbowen.gitlab.io/ordered-inference/).
