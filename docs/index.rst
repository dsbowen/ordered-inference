.. Example project documentation master file, created by
   sphinx-quickstart on Mon Mar 22 14:17:44 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Conditional inference
=====================

A statistics package for comparing multiple policies or treatments.

.. image:: https://gitlab.com/dsbowen/ordered-inference/badges/master/pipeline.svg
   :target: https://gitlab.com/dsbowen/ordered-inference/-/commits/master
.. image:: https://gitlab.com/dsbowen/ordered-inference/badges/master/coverage.svg
   :target: https://gitlab.com/dsbowen/ordered-inference/-/commits/master
.. image:: https://img.shields.io/badge/License-MIT-brightgreen.svg
   :target: https://gitlab.com/dsbowen/ordered-inference/-/blob/master/LICENSE
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/dsbowen%2Fordered-inference/HEAD?urlpath=lab

Quickstart
----------

Click the badge below to launch the Jupyter Binder. The Binder contains a ready-to-use virtual environment and boilerplate code for quantile-unbiased inference.

.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/dsbowen%2Fordered-inference/HEAD?urlpath=lab/tree/examples/boilerplate.ipynb

Installation
------------

.. code-block::

   $ pip install conditional-inference

.. toctree::
   :maxdepth: 2
   :caption: Mixins and utilities
   
   Mixins <base>
   Utilities <utils>

.. toctree::
   :maxdepth: 2
   :caption: Quantile unbiased inference

   Quantile-unbiased estimator <quantile_unbiased.rqu>
   Quantile-unbiased distribution <quantile_unbiased.stats>

.. toctree::
   :maxdepth: 2
   :caption: Bayesian inference

   Mixins <bayes.base>
   Classic Bayes <bayes.classic_bayes>
   Empirical Bayes <bayes.empirical_bayes>
   Hierarchical Bayes <bayes.hierarchical_bayes>

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Citations
---------

.. code-block::

   @software(bowen2021inference
      title={Conditional inference},
      author={Bowen, Dillon},
      year={2021},
      url={https://dsbowen.gitlab.io/ordered-inference},
   )

   @techreport{andrews2019inference,
      title={Inference on winners},
      author={Andrews, Isaiah and Kitagawa, Toru and McCloskey, Adam},
      year={2019},
      institution={National Bureau of Economic Research}
   }
