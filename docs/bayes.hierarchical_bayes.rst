conditional\_inference.bayes.hierarchical\_bayes
================================================

.. automodule:: conditional_inference.bayes.hierarchical_bayes
   :members:
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      HierarchicalBayesBase
      HierarchicalBayesResults
      LinearHierarchicalBayes
   
   

   
   
   



