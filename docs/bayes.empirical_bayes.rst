conditional\_inference.bayes.empirical_bayes
============================================

.. automodule:: conditional_inference.bayes.empirical_bayes
   :members:
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      EmpiricalBayesBase
      LinearEmpiricalBayes
      JamesStein
   
   

   
   
   



