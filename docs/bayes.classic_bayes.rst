conditional\_inference.bayes.classic\_bayes
===========================================

.. automodule:: conditional_inference.bayes.classic_bayes
   :members:
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      ClassicBayesBase
      LinearClassicBayes
   
   

   
   
   



