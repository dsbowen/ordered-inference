from conditional_inference.base import ModelBase, ResultsBase

import os
import pickle

import numpy as np
import pandas as pd
import pytest
import statsmodels.api as sm
from scipy.stats import norm

tol = 0.001

mean = np.arange(3)
cov = np.identity(3)
model = ModelBase(mean, cov)
results = ResultsBase(model)


class TestData:
    @pytest.mark.parametrize("endog_names", [None, "target"])
    def test_endog_names(self, endog_names):
        model = ModelBase(mean, cov, endog_names=endog_names)
        assert model.endog_names == ("y" if endog_names is None else endog_names)

    @pytest.mark.parametrize(
        "exog_names,index",
        [
            (["var0", "var1", "var2"], False),
            (["var0", "var1", "var2"], True),
            (None, False),
        ],
    )
    def test_exog_names(self, exog_names, index):
        if exog_names is None:
            model = ModelBase(mean, cov)
            assert model.exog_names == ["x0", "x1", "x2"]
        else:
            if index:
                model = ModelBase(pd.Series(mean, index=exog_names), cov)
            else:
                model = ModelBase(mean, cov, exog_names=exog_names)
            assert model.exog_names == exog_names

    def test_set_attr(self):
        model = ModelBase(mean, cov)
        exog_names = [f"var{i}" for i in range(mean.shape[0])]
        model.exog_names = exog_names
        assert model.exog_names == exog_names
        assert model.data.exog_names == exog_names


@pytest.fixture(scope="module", params=[True, False])
def ols_results(
    request,
    n_obs_per_policy=100,
    exog_names=["var0", "var1", "var2"],
    endog_name="target",
):
    n_policies = len(exog_names)
    X = pd.DataFrame(
        np.repeat(np.identity(n_policies), n_obs_per_policy, axis=0), columns=exog_names
    )
    y = X @ np.arange(n_policies) + norm.rvs(size=n_policies * n_obs_per_policy)
    y = pd.Series(y, name=endog_name)
    ols_results = sm.OLS(y, X).fit()
    return ols_results if request.param else ols_results.get_robustcov_results()


class TestModel:
    def get_params_cov(self, ols_results):
        params = ols_results.params
        params = params.values if isinstance(params, pd.Series) else params
        cov = ols_results.cov_params()
        cov = cov.values if isinstance(cov, pd.DataFrame) else cov
        return params, cov

    def compare_model_to_ols_results(self, model, ols_results):
        params, cov = self.get_params_cov(ols_results)
        assert ((model.mean - params) ** 2).mean() <= tol
        assert ((model.cov - cov) ** 2).mean() <= tol
        assert model.exog_names == ols_results.model.exog_names
        assert model.endog_names == ols_results.model.endog_names

    @pytest.mark.parametrize("cols", [None, ["var0", "var1", "var2"], [0, 1, 2]])
    def test_from_results(self, ols_results, cols):
        model = ModelBase.from_results(ols_results, cols=cols)
        self.compare_model_to_ols_results(model, ols_results)

    def test_from_csv(self, ols_results, filename="tmp.csv"):
        params, cov = self.get_params_cov(ols_results)
        df = pd.DataFrame(
            np.hstack((params.reshape(-1, 1), cov)),
            columns=[ols_results.model.endog_names] + ols_results.model.exog_names,
        )
        df.to_csv(filename, index=False)
        model = ModelBase.from_csv(filename)
        os.remove(filename)
        self.compare_model_to_ols_results(model, ols_results)

    @pytest.mark.parametrize(
        "cols", [None, "sorted", "x0", ["x2", "x1", "x0"], [2, 1, 0]]
    )
    def test_get_indices(self, cols):
        indices = ModelBase(mean, cov)._get_indices(cols)
        if cols is None:
            assert (indices == [0, 1, 2]).all()
        elif cols == "sorted":
            assert (indices == (-mean).argsort()).all()
        elif cols == "x0":
            assert (indices == [0]).all()
        else:
            assert (indices == [2, 1, 0]).all()


class TestResults:
    def test_conf_int(self):
        with pytest.raises(AttributeError):
            results.conf_int()

    def test_save(self, filename="tmp.p"):
        results.save(filename)
        with open(filename, "rb") as results_file:
            loaded_results = pickle.load(results_file)
        os.remove(filename)
        assert (loaded_results.model.mean == results.model.mean).all()
