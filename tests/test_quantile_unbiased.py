from utils import get_bounds, no_simulations_reason

from conditional_inference.quantile_unbiased.stats import (
    cond_quant_unbiased,
    truncated_cdf,
)
from conditional_inference.quantile_unbiased.rqu import RQU

import logging
import os
from itertools import combinations, product

import numpy as np
import pytest
from scipy.stats import norm
from statsmodels.iolib.summary import Summary

# indicates that simulation tests should be run
# these are long and expensive,
# so you should set this to False when testing only other aspect of the code
run_simulation_tests = os.environ.get("RUN_SIMULATIONS") == "1"
# num simulations to check quantile unbiasedness
n_quantile_unbiased_simulations = 2 ** 5
n_rqu_results_simulations = 2 ** 5  # num simulations to check RQU results
n_projection_results_simulations = 2 ** 5  # num simulations to check projection results
logger = logging.getLogger(__name__)


# simulation parameters for rank, beta, n_policies
quantile_unbiased_params = list(product((0, "middle"), (0, 0.005), (10, 20)))


@pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
@pytest.mark.parametrize("rank,beta,n_policies", quantile_unbiased_params)
def test_quantile_unbiased(
    rank,
    beta,
    n_policies,
    quantiles=[0.25, 0.5, 0.75],
    n_simulations=n_quantile_unbiased_simulations,
):
    def compute_quantiles():
        y = norm().rvs(n_policies)
        sigma = np.identity(n_policies)
        dist = RQU(y, sigma).get_distribution(rank=rank, beta=beta)
        return dist.ppf(quantiles)

    def test_quantile(q, observed):
        logger.info(
            f"True value is below the {q} quantile estimate with probability {observed}"
        )
        lower_bound, upper_bound = get_bounds(n_simulations, q)
        max_error = beta * max(q, 1 - q)
        return lower_bound - max_error <= observed <= upper_bound + max_error

    if rank == "middle":
        rank = int(n_policies / 2)
    observed_quantiles = np.array([compute_quantiles() for _ in range(n_simulations)])
    observed_quantiles = (observed_quantiles > 0).mean(axis=0)
    for q, observed in zip(quantiles, observed_quantiles):
        test_quantile(q, observed)


@pytest.fixture(scope="module", params=list(product((0, 0.005), (10,))))
def rqu_results(request, n_simulations=n_rqu_results_simulations):
    def get_results():
        y = norm().rvs(n_policies)
        y = y[(-y).argsort()]
        sigma = np.identity(n_policies)
        return RQU(y, sigma).fit(beta=beta)

    beta, n_policies = request.param
    return beta, n_policies, [get_results() for _ in range(n_simulations)]


class TestRQUResults:
    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    @pytest.mark.parametrize("rank", (0, "middle"))
    def test_conf_int(self, rank, rqu_results, alpha=0.05):
        beta, n_policies, results = rqu_results
        if rank == "middle":
            rank = int(n_policies / 2)
        conf_int = np.array(
            [result.conf_int(cols=[rank], alpha=alpha).squeeze() for result in results]
        )
        observed_alpha = 1 - ((conf_int[:, 0] < 0) & (0 < conf_int[:, 1])).mean()
        logger.info(f"{1-alpha} CI coverage: {1-observed_alpha}")
        lower_bound, upper_bound = get_bounds(len(results), alpha)
        assert lower_bound <= observed_alpha <= upper_bound

    @pytest.mark.skipif(
        not run_simulation_tests,
        reason=no_simulations_reason,
    )
    @pytest.mark.parametrize("rank", (0, "middle"))
    def test_pvalues(self, rank, rqu_results, alphas=(0.01, 0.05, 0.1)):
        beta, n_policies, results = rqu_results
        if rank == "middle":
            rank = int(n_policies / 2)
        pvalues = np.array([result.pvalues[rank] for result in results])
        for alpha in alphas:
            fp_rate = (pvalues < alpha).mean()
            logger.info(f"FP rate for alpha={alpha}: {fp_rate:.3f}")
            lower_bound, upper_bound = get_bounds(len(results), alpha)
            # pvalues are already adjusted downward to account for possible error
            lower_bound -= 2 * beta * max(alpha, 1 - alpha)
            assert lower_bound <= fp_rate <= upper_bound


@pytest.fixture(
    scope="module",
    params=[
        10,
    ],
)
def projection_results(request, n_simulations=n_projection_results_simulations):
    def get_results():
        y = norm().rvs(n_policies)
        y = y[(-y).argsort()]
        sigma = np.identity(n_policies)
        return RQU(y, sigma).fit(projection=True)

    n_policies = request.param
    return n_policies, [get_results() for _ in range(n_simulations)]


class TestProjectionResults:
    @pytest.mark.skipif(
        not run_simulation_tests,
        reason=no_simulations_reason,
    )
    @pytest.mark.parametrize("rank", (0, "middle"))
    def test_conf_int(self, rank, projection_results, alpha=0.05):
        n_policies, results = projection_results
        if rank == "middle":
            rank = int(n_policies / 2)
        conf_int = np.array(
            [result.conf_int(cols=[rank], alpha=alpha).squeeze() for result in results]
        )
        observed_alpha = 1 - ((conf_int[:, 0] < 0) & (0 < conf_int[:, 1])).mean()
        logger.info(f"{1-alpha} CI coverage: {1-observed_alpha}")
        _, upper_bound = get_bounds(len(results), alpha)
        # projection results have > 1 - alpha coverage, so only test against upper bound
        assert observed_alpha <= upper_bound

    @pytest.mark.skipif(
        not run_simulation_tests,
        reason=no_simulations_reason,
    )
    @pytest.mark.parametrize("rank", (0, "middle"))
    def test_pvalues(self, rank, projection_results, alphas=(0.01, 0.05, 0.1)):
        n_policies, results = projection_results
        if rank == "middle":
            rank = int(n_policies / 2)
        pvalues = np.array([result.pvalues[rank] for result in results])
        for alpha in alphas:
            fp_rate = (pvalues < alpha).mean()
            logger.info(f"FP rate for alpha={alpha}: {fp_rate:.3f}")
            _, upper_bound = get_bounds(len(results), alpha)
            assert fp_rate <= upper_bound


@pytest.fixture(
    scope="module", params=[dict(beta=0), dict(beta=0.005), dict(projection=True)]
)
def scaling_results(request):
    def get_results(scale):
        x = scale * np.arange(-1, 2, step=1)
        sigma = scale ** 2 * np.identity(3)
        return scale, RQU(x, sigma).fit(**request.param)

    scales = np.array([0.1, 1, 10])
    results = [get_results(scale) for scale in scales]
    return list(combinations(results, 2))


class TestScaling:
    param_scaling_tol = 1e-2
    ci_scaling_tol = 2e-1
    pvalue_tol = 2e-2

    def test_params(self, scaling_results):
        # test that point estimates scale as expected
        for (scale0, result0), (scale1, result1) in scaling_results:
            params = (result1.params / result0.params)[result0.params != 0]
            logger.info(
                f"scaling error for params: {abs(params * scale0 / scale1 - 1)}"
            )
            assert (abs(params * scale0 / scale1 - 1) < self.param_scaling_tol).all()

    def test_conf_int(self, scaling_results):
        # test that confidence intervals scale as expected
        for (scale0, result0), (scale1, result1) in scaling_results:
            relative_ci = result1.conf_int() / result0.conf_int()
            logger.info(
                f"scaling error for CIs: {abs(relative_ci * scale0 / scale1 - 1)}"
            )
            assert (abs(relative_ci * scale0 / scale1 - 1) < self.ci_scaling_tol).all()

    def test_pvalues(self, scaling_results):
        # test that pvalues are unaffected by scaling
        for (_0, result0), (_1, result1) in scaling_results:
            pvalue_err = abs(result0.pvalues - result1.pvalues)
            logger.info(f"scaling error for pvalues: {pvalue_err}")
            assert (pvalue_err < self.pvalue_tol).all()


class TestTruncatedCDF:
    def test_lower_bound_above_upper_bound(self):
        with pytest.raises(ValueError):
            truncated_cdf(0, truncation_set=[(1, 0)])

    def test_overlapping_intervals(self):
        with pytest.raises(ValueError):
            truncated_cdf(0, truncation_set=[(0, 2), (1, 3)])

    @pytest.mark.parametrize(
        "x,interval",
        [(0, (100, 200)), (150, (100, 200)), (0, (-200, -100)), (-150, (-200, -100))],
    )
    def test_Z_eq_0(self, x, interval):
        # test that truncated_cdf handles rounding errors gracefully
        with pytest.warns(RuntimeWarning):
            cdf = truncated_cdf(x, truncation_set=[interval])
        if interval[0] > 0:
            assert cdf == (0 if interval[0] > x else 1)
        else:
            assert cdf == (0 if interval[1] > x else 1)


class TestCondQuantUnbiased:
    pdf_tol = 1e-4

    def test_pdf(self):
        # distribution should converge to normal as truncation set widens
        dist = cond_quant_unbiased(0)
        x = np.linspace(-3, 3)
        error = abs(dist.pdf(x) - norm.pdf(x))
        mean_error = error.mean()
        logger.info(f"Mean pdf error: {mean_error}")
        assert mean_error < self.pdf_tol

    @pytest.mark.parametrize("bounds", [(100, 200), (-200, -100)])
    def test_boundary_edge_cases(self, bounds):
        with pytest.warns(RuntimeWarning):
            dist = cond_quant_unbiased(0, bounds=bounds)

        cdf = dist.cdf([0, (bounds[0] + bounds[1]) / 2])
        ppf = dist.ppf([0, 0.5, 1])
        if bounds[0] > 0:
            assert (cdf == np.array([0, 1])).all()
            assert (ppf == np.array([bounds[0], bounds[0], bounds[1]])).all()
        else:
            assert (cdf == np.array([1, 0])).all()
            assert (ppf == np.array([bounds[0], bounds[1], bounds[1]])).all()


class TestRQU:
    # tests aspects of RQU not covered in simulations

    def test_projection_quantile(self):
        # test when alpha == 0
        # see simulation tests for more rigorous tests when alpha != 0
        rqu = RQU(np.arange(3), np.identity(3))
        assert rqu.compute_projection_quantile(alpha=0) == np.inf

    def test_s_V_condition(self):
        # this condition applies with cov(x_i,x_j) == var(x_i)
        # when it fails, the truncation set is empty
        # see paper for mathematical detail
        with pytest.raises(ValueError):
            RQU(np.arange(2), np.ones((2, 2))).get_distribution(rank=1)

    @pytest.mark.parametrize("rank", ["invalid_rank", "floor", "ceil"])
    def test_rank_arguments(self, rank):
        def get_truncation_interval(dist):
            truncation_set = dist.truncated_cdf_kwargs["truncation_set"]
            a, b = list(zip(*truncation_set))
            return min(a), max(b)

        rqu = RQU(np.arange(2), np.identity(2))

        if rank not in ("floor", "ceil"):
            with pytest.raises(ValueError):
                rqu.get_distributions(rank=rank)
            return

        with pytest.raises(ValueError):
            rqu.get_distribution(rank=rank)

        dists = rqu.get_distributions(rank=rank)
        truncation_sets = [get_truncation_interval(dist) for dist in dists]
        if rank == "floor":
            assert truncation_sets == [(-np.inf, np.inf), (0, np.inf)]
        else:  # rank == "ceil"
            assert truncation_sets == [(-np.inf, 1), (-np.inf, np.inf)]

    def test_get_distribution_default(self, npolicies=3):
        rqu = RQU(np.arange(npolicies), np.identity(npolicies))
        assert rqu.get_distribution().y == npolicies - 1


@pytest.fixture(
    scope="module", params=[dict(beta=0), dict(beta=0.005), dict(projection=True)]
)
def plot_and_summary_result(request, n_policies=3):
    return RQU(np.arange(n_policies), np.identity(n_policies)).fit(**request.param)


class TestPlotAndSummary:
    # nb. the correctness of the numbers used to generate plots and summaries has been verified in previous tests
    # these tests simply ensure that the data can be plotted and summarized without error

    def test_plot(self, plot_and_summary_result):
        plot_and_summary_result.point_plot()

    def test_summary(self, plot_and_summary_result):
        assert isinstance(plot_and_summary_result.summary(), Summary)
