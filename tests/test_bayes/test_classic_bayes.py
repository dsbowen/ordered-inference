from utils import (
    no_simulations_reason,
    make_simulation_params,
    get_results,
    run_conf_int,
    run_pvalues,
    compare_mse,
    compare_likelihood,
)

from conditional_inference.bayes.classic_bayes import LinearClassicBayes

import os
from itertools import product

import numpy as np
import pytest
from scipy.stats import multivariate_normal
from statsmodels.iolib.summary import Summary

# indicates that simulation tests should be run
# these are long and expensive,
# so you should set this to False when testing only other aspect of the code
run_simulation_tests = os.environ.get("RUN_SIMULATIONS") == "1"
n_simulations = 2 ** 5


@pytest.fixture(scope="module", params=list(product((5,), (0.1, 1, 10))))
def simulation_params(request, n_simulations=n_simulations):
    return make_simulation_params(request, n_simulations)


@pytest.fixture(scope="module")
def oos_means(simulation_params):
    # simulates sampling means from a test (oos = out of sample) set
    true_means, covs = simulation_params[1], simulation_params[3]
    return [
        multivariate_normal(true_mean, cov).rvs()
        for true_mean, cov in zip(true_means, covs)
    ]


@pytest.fixture(scope="module")
def results(simulation_params):
    return get_results(
        LinearClassicBayes, simulation_params, prior_cov=simulation_params[0]
    )


@pytest.fixture(scope="module")
def conventional_results(simulation_params):
    return get_results(LinearClassicBayes, simulation_params, prior_cov=np.inf)


@pytest.fixture(scope="module")
def baseline_results(simulation_params):
    return get_results(LinearClassicBayes, simulation_params, prior_cov=0)


class TestSimulations:
    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    def test_conf_int(self, results, simulation_params):
        run_conf_int(results, simulation_params)

    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    def test_pvalues(self, results, simulation_params):
        run_pvalues(results, simulation_params)

    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    def test_mse(
        self, results, baseline_results, conventional_results, simulation_params
    ):
        compare_mse(
            results,
            simulation_params,
            baseline_results=baseline_results,
            conventional_results=conventional_results,
        )

    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    def test_likelihood(
        self,
        results,
        simulation_params,
        oos_means,
        baseline_results,
        conventional_results,
    ):
        compare_likelihood(
            results,
            simulation_params,
            oos_means,
            baseline_results=baseline_results,
            conventional_results=conventional_results
        )


class TestPlotAndSummary:
    def test_point_plot(self, results):
        results[0].point_plot()

    def test_rank_matrix_plot(self, results):
        results[0].rank_matrix_plot()

    def test_summary(self, results):
        assert isinstance(results[0].summary(), Summary)
