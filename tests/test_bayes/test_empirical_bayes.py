from utils import (
    no_simulations_reason,
    make_simulation_params,
    compare_mse,
    get_results,
)

from conditional_inference.bayes.classic_bayes import LinearClassicBayes
from conditional_inference.bayes.empirical_bayes import LinearEmpiricalBayes

import os
from itertools import product

import numpy as np
import pytest

run_simulation_tests = os.environ.get("RUN_SIMULATIONS") == "1"
n_simulations = 2 ** 5


@pytest.fixture(scope="module", params=list(product((5,), (0.1, 1, 10))))
def simulation_params(request, n_simulations=n_simulations):
    return make_simulation_params(request, n_simulations)


@pytest.fixture(scope="module")
def results(simulation_params):
    return get_results(LinearEmpiricalBayes, simulation_params)


@pytest.fixture(scope="module")
def conventional_results(simulation_params):
    return get_results(LinearClassicBayes, simulation_params, prior_cov=np.inf)


class TestSimulations:
    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    def test_mse(self, results, simulation_params, conventional_results):
        compare_mse(
            results,
            simulation_params,
            conventional_results=conventional_results,
        )
