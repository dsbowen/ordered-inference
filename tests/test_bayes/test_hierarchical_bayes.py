from utils import (
    no_simulations_reason,
    make_simulation_params,
    compare_mse,
    get_results,
    run_conf_int,
    run_pvalues,
)

from conditional_inference.bayes.classic_bayes import LinearClassicBayes
from conditional_inference.bayes.hierarchical_bayes import LinearHierarchicalBayes

import os
from itertools import product

import numpy as np
import pytest
from scipy.stats import loguniform

run_simulation_tests = os.environ.get("RUN_SIMULATIONS") == "1"
n_simulations = 2 ** 5
prior_cov_params_distribution = loguniform(1e-1, 1e1)


@pytest.fixture(
    scope="module", params=list(product((5,), (prior_cov_params_distribution,)))
)
def simulation_params(request, n_simulations=n_simulations):
    return make_simulation_params(request, n_simulations)


@pytest.fixture(scope="module")
def results(simulation_params):
    return get_results(
        LinearHierarchicalBayes,
        simulation_params,
        prior_cov_params_distribution=prior_cov_params_distribution,
    )


@pytest.fixture(scope="module")
def conventional_results(simulation_params):
    return get_results(LinearClassicBayes, simulation_params, prior_cov=np.inf)


class TestSimulations:
    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    def test_conf_int(self, results, simulation_params):
        run_conf_int(results, simulation_params)

    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    def test_pvalues(self, results, simulation_params):
        run_pvalues(results, simulation_params)

    @pytest.mark.skipif(not run_simulation_tests, reason=no_simulations_reason)
    def test_mse(self, results, simulation_params, conventional_results):
        compare_mse(
            results,
            simulation_params,
            conventional_results=conventional_results,
        )
