import logging

import numpy as np
from scipy.stats import binom, multivariate_normal

no_simulations_reason = "not running tests that use long-running simulations"
logger = logging.getLogger(__name__)


def get_bounds(n_simulations, expected_p, tol=0.995):
    # get a range of results under expected behavior by constructing a 1-tol CI
    return (
        binom(n_simulations, expected_p).ppf((1 - tol) / 2) / n_simulations,
        binom(n_simulations, expected_p).ppf(1 - (1 - tol) / 2) / n_simulations,
    )


def make_simulation_params(request, n_simulations, homoskedastic=False):
    def make_params():
        true_mean = multivariate_normal(prior_mean, sample_prior_cov()).rvs()
        if homoskedastic:
            cov = np.identity(n_policies)
        else:
            cov = np.diag(np.random.uniform(1, 2, size=n_policies))
        sample_mean = multivariate_normal(true_mean, cov).rvs()
        return true_mean, sample_mean, cov

    def sample_prior_cov():
        if hasattr(prior_cov, "rvs"):
            # prior_cov is a distribution
            return prior_cov.rvs() * np.identity(n_policies)
        else:
            return prior_cov

    n_policies, prior_cov = request.param
    prior_mean = np.zeros(n_policies)
    if np.isscalar(prior_cov):
        prior_cov = prior_cov * np.identity(n_policies)
    true_means, sample_means, covs = zip(*[make_params() for _ in range(n_simulations)])
    return prior_cov, np.array(true_means), np.array(sample_means), np.array(covs)


def get_results(model_cls, simulation_params, **kwargs):
    sample_means, covs = simulation_params[2:]
    results = []
    for sample_mean, cov in zip(sample_means, covs):
        model = model_cls(sample_mean, cov, **kwargs)
        results.append(model.fit())
    return results


def run_conf_int(results, simulation_params, alphas=(0.01, 0.05, 0.1)):
    true_means = simulation_params[1]
    for alpha in alphas:
        conf_int = np.array([result.conf_int(alpha=alpha) for result in results])
        observed_alpha = (
            (true_means < conf_int[:, :, 0]) | (conf_int[:, :, 1] < true_means)
        ).mean()
        logger.info(f"{1-alpha} CI coverage: {1-observed_alpha}")
        _, upper_bound = get_bounds(true_means.size, alpha)
        assert observed_alpha <= upper_bound


def run_pvalues(results, simulation_params, alphas=(0.01, 0.05, 0.1)):
    true_means = simulation_params[1]
    true_mean_lt_0 = true_means < 0
    pvalues = np.array([result.pvalues for result in results])
    pvalues = pvalues[true_mean_lt_0]
    for alpha in alphas:
        fp_rate = (pvalues < alpha).sum() / true_mean_lt_0.sum()
        logger.info(f"FP rate for alpha={alpha}: {fp_rate:.3f}")
        _, upper_bound = get_bounds(pvalues.size, alpha)
        assert fp_rate <= upper_bound


def compare_mse(
    results, simulation_params, baseline_results=None, conventional_results=None
):
    def compute_mse(results):
        params = np.array([result.params for result in results])
        return ((params - true_means) ** 2).mean(axis=1)

    def compare(comparison_results, name):
        comparison_mse = compute_mse(comparison_results)
        comparison = (mse <= comparison_mse).mean()
        logger.info(f"Pr Bayesian MSE < {name} MSE: {comparison}")
        assert lower_bound <= comparison

    true_means = simulation_params[1]
    mse = compute_mse(results)
    lower_bound, _ = get_bounds(len(results), 0.5)
    if baseline_results is not None:
        compare(baseline_results, "baseline")
    if conventional_results is not None:
        compare(conventional_results, "conventional")

def compare_likelihood(
    results, simulation_params, oos_means, baseline_results=None, conventional_results=None
):
    def compute_likelihood(results):
        return np.array(
            [
                result.likelihood(oos_mean, cov)
                for result, oos_mean, cov in zip(results, oos_means, covs)
            ]
        )

    def compare(comparison_results, name):
        comparison_likelihood = compute_likelihood(comparison_results)
        comparison = (likelihood >= comparison_likelihood).mean()
        logger.info(f"Pr Bayesian likelihood > {name} likelihood: {comparison}")
        assert lower_bound <= comparison

    covs = simulation_params[3]
    likelihood = compute_likelihood(results)
    lower_bound, _ = get_bounds(len(results), 0.5)
    if baseline_results is not None:
        compare(baseline_results, "baseline")
    if conventional_results is not None:
        compare(conventional_results, "conventional")
