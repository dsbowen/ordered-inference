import os

import numpy as np
import pandas as pd

filenames = [
    ("penn_medicine.csv", "penn_medicine_reconstructed.csv"),
    ("walmart.csv", "walmart_reconstructed.csv")
]
datapath = "data"
if not os.path.exists(datapath):
    os.mkdir(datapath)
realpath = os.path.dirname(os.path.realpath(__file__))

def reconstruct_dataset(infile, outfile):
    def gen_y_vector(row):
        n_hits = round(row["mean"] * row.n_obs)
        return n_hits * [1] + int(row.n_obs - n_hits) * [0]

    df = pd.read_csv(os.path.join(realpath, infile))
    X = np.hstack((df[["n_texts", "n_days", "control"]], np.arange(len(df)).reshape(-1, 1)))
    X = X.repeat(df.n_obs, axis=0)
    y = np.array(df.apply(gen_y_vector, axis=1).to_list()).flatten()
    result_df = pd.DataFrame(
        np.hstack((X, y.reshape(-1, 1))),
        columns=["n_texts", "n_days", "control", "arm", "y"]
    )
    result_df.to_csv(os.path.join(datapath, outfile), index=False)

def make_arm(row):
    return f"ask={row.ask}_ratio={row.ratio}_size={row['size']}"

if __name__ == "__main__":
    # reconstruct flu datasets
    for infile, outfile in filenames:
        reconstruct_dataset(infile, outfile)
    # clean Karlan and List (charity) dataset
    df = pd.read_stata(os.path.join(datapath, "AERtables1-5.dta"))
    df["arm"] = df.apply(make_arm, axis=1)
    df["y"] = df.amount
    df.to_csv(os.path.join(datapath, "charity.csv"), index=False)
    # clean DellaVigna and Pope (effort) dataset
    df = pd.read_stata(os.path.join(datapath, "MTurkCleanedData.dta"))
    df["arm"] = df.treatmentname
    df["control"] = df.treatmentname == "No Payment"
    df["y"] = df.buttonpresses
    df.to_csv(os.path.join(datapath, "effort.csv"), index=False)
